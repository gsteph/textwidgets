#include "stdafx.h"

struct CColouredStatic : public CStatic
{
	bool hover;
	bool sizable;
	CColouredStatic(CRect r, bool s){
		hover = false;
		sizable = s;
		CStatic();
	}
	afx_msg BOOL OnEraseBkgnd(CDC* pDC){
		return FALSE;
	}
	HBRUSH CtlColor(CDC* pDC, UINT nCtlColor) {
		if (hover && GetKeyState(VK_LCONTROL) & 0x1000) pDC->SetBkColor(RGB(70,70,70));
		else pDC->SetBkColor(RGB(25,25,25));
		return (HBRUSH)GetStockObject(NULL_BRUSH);  
	}
	void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct){
		CRect r(lpDrawItemStruct->rcItem);
		CMemDC1 mdc(CDC::FromHandle(lpDrawItemStruct->hDC));

		mdc->SetTextColor(RGB(255,255,255));
		mdc->SelectObject(GetFont());
		CString s;
		GetWindowText(s);
		
#ifdef wordwrap
		CString n;
		int i = r.Width()/mdc->GetTextExtent("i").cx;
		int k = 0;
		for (int j = 0; j < s.GetLength(); j++){
			k++;
			if (s[j] == '\n') {
				k = 0;
				continue;
			}
			n+=s[j];
			if (k==i) n+='\n';
		}
#endif
		mdc->DrawText(s, r, NULL);
		
		if (hover && GetKeyState(VK_LCONTROL) & 0x1000 && sizable){
			int cx = r.right - r.left;
			int cy = r.bottom - r.top;
			mdc->FillRect(CRect(cx-10, cy-10, cx, cy), new CBrush(RGB(255,0,0))); 
		}
	}
	DECLARE_MESSAGE_MAP()
};
BEGIN_MESSAGE_MAP(CColouredStatic, CStatic)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

class CWidget : public CFrameWnd{
public:
	CFont font;
	CColouredStatic *Edit;
	PyGILState_STATE gstate;
	PyObject* update;
	PyObject *module;
	CStringA file;
	bool sizable;
	bool mousein;
	CPoint deltacur;
	CWnd *main;
	CWidget(CString _file, CPoint pt, CWnd *m){
		Edit = new CColouredStatic(CRect(0, 0, 300, 100), sizable);
		main = m;
		file = _file.Left(_file.GetLength()-3);

		Create(NULL, "TextWidgets", WS_POPUP | WS_VISIBLE);
		ModifyStyleEx(WS_EX_CLIENTEDGE | WS_EX_APPWINDOW, WS_EX_COMPOSITED, SWP_FRAMECHANGED);

		Edit->Create("", WS_VISIBLE | SS_OWNERDRAW, CRect(0, 0, 300, 100), this);
		font.CreatePointFont(100, "Consolas");
		Edit->SetFont(&font);
		SetWindowPos(&wndTopMost , pt.x*7, pt.y*15, NULL, NULL, /*SWP_NOREPOSITION |*/ SWP_NOSIZE);
		module = 0;
		PostMessage(WM_RELOAD);
	}
	afx_msg void OnMouseLeave(){
		Edit->hover = false;
		Edit->Invalidate();
	}
	afx_msg void OnMouseMove(UINT nFlags, CPoint point){
		if (!Edit->hover){
			Edit->hover = true;
			TRACKMOUSEEVENT tme;
			tme.cbSize = sizeof(tme);
			tme.hwndTrack = m_hWnd;
			tme.dwFlags = TME_LEAVE;
			TrackMouseEvent(&tme);
			Edit->Invalidate();
		}

		if (nFlags & MK_CONTROL) {
			CRect r;
			GetWindowRect(r);
			r = CRect(r.Width()-10, r.Height()-10, r.Width(), r.Height());
			if (r.PtInRect(point) && sizable){
				SetCursor(LoadCursor(NULL, IDC_SIZENWSE));
				if (nFlags & MK_LBUTTON)
					DefWindowProc(WM_SYSCOMMAND, SC_SIZE + 8, 0);
			}else 
				if (nFlags & MK_LBUTTON) {
					POINT p;
					GetWindowRect(r);
					GetCursorPos(&p);
					deltacur.x = p.x - r.left;
					deltacur.y = p.y - r.top;
					DefWindowProc(WM_SYSCOMMAND, SC_MOVE + 1, 0);
				}
		}	
		Edit->Invalidate();
	}
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point){
		if (nFlags & MK_CONTROL) {
			CString a = CString("C:\\TextWidgets\\") + file + CString(".py");
			HINSTANCE i = ShellExecute(this->m_hWnd, "Edit with IDLE", a, NULL, NULL, SW_SHOWNORMAL);
			int b = 1;
		}
	}
	afx_msg BOOL OnEraseBkgnd(CDC* pDC){
		return FALSE;
	}
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags){
		Edit->Invalidate();
	}
	afx_msg void OnSize(UINT nType, int cx, int cy){
		if (::IsWindow(Edit->m_hWnd)) {
			Edit->MoveWindow(0, 0, cx, cy);
		}
	}
	afx_msg void OnSizing(UINT nSide, LPRECT lpRect){
		int cx = lpRect->right - lpRect->left;
		int cy = lpRect->bottom - lpRect->top;
		cx = (cx / 7) + ((cx % 7) > 3);
		cy = (cy / 15) + ((cy % 15) > 7);
		lpRect->right = lpRect->left + cx*7;
		lpRect->bottom = lpRect->top + cy*15;
	}
	afx_msg void OnMoving(UINT nSide, LPRECT lpRect){
		POINT p;
		GetCursorPos(&p);
		int cx = lpRect->right - lpRect->left;
		int cy = lpRect->bottom - lpRect->top;
		lpRect->top = p.y - deltacur.y;
		lpRect->left = p.x - deltacur.x;
		lpRect->top = 15*((lpRect->top / 15) + ((lpRect->top % 15) > 7));
		lpRect->left = 7*((lpRect->left / 7) + ((lpRect->left % 7) > 3));
		lpRect->right = lpRect->left+cx;
		lpRect->bottom = lpRect->top+cy;
	}
	afx_msg void OnMove(int x, int y){
		main->PostMessageA(WM_UPDATECONF, (WPARAM)&(file), MAKELPARAM(x/7,y/15));
	}
	afx_msg void OnTimer( UINT_PTR nIDEvent ){
		char *err;
		gstate = PyGILState_Ensure();
		
		PyObject *result = PyObject_CallObject(update, NULL);
		if (!result){
			err = "Calling update() failed. Check error log.";
			PyErr_Print();
			PyRun_SimpleString("sys.stderr.flush()");
			KillTimer(0);
			goto error;
		}
		//if (PyUnicode_Check(result)){
			//wchar_t str[4096];
			//MultiByteToWideChar(CP_UTF8, MB_PRECOMPOSED, 
			//PyUnicode_AsWideChar((PyUnicodeObject*)result, str, 4096);
			//::SetWindowTextW(Edit->m_hWnd, (wchar_t*)str);
		//}else if (PyString_Check(result)){
//			Edit->SetWindowTextA(PyString_AsString(result));
		if (PyString_Check(result)){
			char *str = PyString_AsString(result);
			wchar_t wstr[4096] = {0};
			MultiByteToWideChar(CP_UTF8, NULL, str, strlen(str), wstr, 4096);
			::SetWindowTextW(Edit->m_hWnd, wstr);
		}else if(PyUnicode_Check(result)){
			wchar_t str[4096];
			PyUnicode_AsWideChar((PyUnicodeObject*)result, str, 4096);
			::SetWindowTextW(Edit->m_hWnd, (wchar_t*)str);
		}else{
			err = "update() did not return a str";
			goto error;
		}
		
		PyGILState_Release(gstate);
		return;
error:
		Edit->SetWindowTextA(err);
		this->SetWindowPos(NULL, NULL, NULL, 400, 50, SWP_NOACTIVATE | SWP_NOREPOSITION | SWP_NOMOVE);
		PyGILState_Release(gstate);
	}
	afx_msg void OnGetMinMaxInfo( MINMAXINFO* lpMMI ){
		lpMMI->ptMinTrackSize.x = 9;
		lpMMI->ptMinTrackSize.y = 17;
	}
	afx_msg LRESULT OnReload(WPARAM wParam, LPARAM lParam){
		char *err;
		gstate = PyGILState_Ensure();
		PyObject *modulesdict = PyObject_GetAttrString(PyImport_AddModule("sys"), "modules");
		PyDict_DelItemString(modulesdict, file);
		PyErr_Clear();
		module = PyImport_ImportModule(file);
		if (module) update = PyDict_GetItemString (PyModule_GetDict(module), "update");
		else{
			err = "Module failed to load. Check error log.";
			PyErr_Print();
			PyRun_SimpleString("sys.stderr.flush()");
			goto error;
		};

		if (!update) {
			err = "update() missing.";
			goto error;
		}

		PyObject *objh = PyObject_GetAttrString(module, "_height");
		PyObject *objw = PyObject_GetAttrString(module, "_width");
		PyObject *obji = PyObject_GetAttrString(module, "_interval");

		if (!objh && !objw) {
			SetWindowPos(NULL, NULL, NULL, 400, 100, SWP_NOACTIVATE | SWP_NOREPOSITION | SWP_NOMOVE);
			sizable = true;
			Edit->sizable = true;
		}
		else if (!objh || !objw) {
			err = "_width/_height missing.";
			goto error;
		} else {
			sizable = false;
			Edit->sizable = false;
			int h = PyInt_AsLong(objh);
			int w = PyInt_AsLong(objw);
			if (h == -1 || w == -1) {
				err = "_width/_height not integers";
				goto error;
			} else SetWindowPos(NULL, NULL, NULL, w*7, h*15, SWP_NOACTIVATE | SWP_NOREPOSITION | SWP_NOMOVE);
		}

		if (!obji) {
			err = "_interval missing";
			goto error;
		}
		PyErr_Clear();
		double i = PyFloat_AsDouble(obji);
		if (PyErr_Occurred()){
			err = "_interval not a float";
			goto error;
		}
		if (i!=-1) SetTimer(0, (int)(i*1000), NULL);
		OnTimer(0);
		PyGILState_Release(gstate);
		return TRUE;
error:
		Edit->SetWindowText(err);
		SetWindowPos(NULL, NULL, NULL, 400, 100, SWP_NOACTIVATE | SWP_NOREPOSITION | SWP_NOMOVE);
		KillTimer(0);
		PyGILState_Release(gstate);
		return TRUE;
	}
	afx_msg void OnClose(){
		return;
	}
	afx_msg LRESULT OnActualClose(WPARAM wParam, LPARAM lParam){
		DestroyWindow();
		return TRUE;
	}
	DECLARE_MESSAGE_MAP()
};

BEGIN_MESSAGE_MAP(CWidget, CFrameWnd)
	ON_WM_ERASEBKGND()
	ON_WM_GETMINMAXINFO()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSELEAVE()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_KEYDOWN()
	ON_WM_SIZE()
	ON_WM_SIZING()
	ON_WM_MOVING()
	ON_WM_MOVE()
	ON_WM_WINDOWPOSCHANGING()
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_RELOAD, OnReload)
	ON_MESSAGE(WM_ACTUALCLOSE, OnActualClose)
END_MESSAGE_MAP()

class CWidgetThread : public CWinThread{
public:
	CString file;
	CPoint pt;
	CWnd *main;
	BOOL InitInstance(){
		CWidget *Frame = new CWidget(file, pt, main);
		m_pMainWnd = Frame;
		
		HWND ProgmanHwnd = ::FindWindowEx(  ::FindWindowEx(  ::FindWindow("Progman", "Program Manager"), NULL, "SHELLDLL_DefView", ""), NULL, "SysListView32", "FolderView");
		SetParent(Frame->m_hWnd, ProgmanHwnd);
		
		Frame->ShowWindow(SW_NORMAL);

		return TRUE;
	}
	DECLARE_DYNCREATE(CWidgetThread)
};
IMPLEMENT_DYNCREATE(CWidgetThread, CWinThread)

#ifdef DEBUG
BOOL WINAPI HandlerRoutine(DWORD dwCtrlType){
	if (dwCtrlType == CTRL_CLOSE_EVENT)
		FreeConsole();
	return TRUE;
}
#endif

class CMainWnd : public CFrameWnd{
public:
	NOTIFYICONDATA icon;
	map<CString, CPoint> *config;
	CMainWnd(map<CString, CPoint> *c){
		config = c;
		Create(NULL, "CMainWnd", WS_POPUP);
		icon.cbSize = sizeof(NOTIFYICONDATA);
		icon.hWnd = this->m_hWnd;
		icon.uID = 0;
		icon.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP;
		icon.uVersion = NOTIFYICON_VERSION;
		icon.uCallbackMessage = WM_NOTIFYICON;
		icon.hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON1), IMAGE_ICON, 16, 16, NULL);
		strcpy(icon.szTip, "TextWidgets");
		Shell_NotifyIcon(NIM_ADD, &icon);
	}
	afx_msg LRESULT OnNotifyIcon(WPARAM wParam, LPARAM lParam){
		switch(LOWORD(lParam)){
		case WM_RBUTTONUP:
			POINT p;
			GetCursorPos(&p);
			CMenu menu;
			menu.CreatePopupMenu();
			menu.AppendMenuA(MF_POPUP, (UINT) 0, "Exit");
			menu.TrackPopupMenu(NULL, p.x, p.y, this);
			DestroyMenu(menu);
		}
		return TRUE;
	}
	afx_msg BOOL OnCommand(WPARAM wParam, LPARAM lParam){
		switch(LOWORD(lParam)){
		case 0:
			Shell_NotifyIcon(NIM_DELETE, &icon);
			PostMessage(WM_CLOSE);
			return TRUE;
		default:
			return FALSE;
		}
	}
	afx_msg void OnClose(){
		CStdioFile f;
		if (f.Open("C:\\TextWidgets\\widgets.cfg", CFile::modeWrite | CFile::shareDenyNone)){
			for (map<CString, CPoint>::iterator i = (*config).begin(); i != (*config).end(); i++){
				CString tmp;
				tmp.Format("%s %d %d\n", (*i).first, (*i).second.x, (*i).second.y);
				f.WriteString(tmp);
			}
		}
		DestroyWindow();
	}
	afx_msg LRESULT OnUpdateConf(WPARAM wParam, LPARAM lParam){
		CString file = *(CString*)wParam;
		(*config)[file+".py"] = CPoint(LOWORD(lParam),HIWORD(lParam));
		return TRUE;
	}
	DECLARE_MESSAGE_MAP()
};
BEGIN_MESSAGE_MAP(CMainWnd, CFrameWnd)
	ON_MESSAGE(WM_NOTIFYICON, OnNotifyIcon)
	ON_MESSAGE(WM_UPDATECONF, OnUpdateConf)
	ON_WM_CLOSE()
END_MESSAGE_MAP()

void moduleWatch(pair<map<CString, CWidgetThread*>*, map<CString, CPoint>* > *p){
	map<CString, CWidgetThread*> modules = *(p->first);
	map<CString, CPoint> config = *(p->second);
	HANDLE hDir = CreateFile("C:\\TextWidgets", FILE_LIST_DIRECTORY, FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, NULL);
	FILE_NOTIFY_INFORMATION *pInfo;
	TCHAR buffer[1024] = {0};
	DWORD BytesReturned;
	while( ReadDirectoryChangesW(hDir, &buffer, sizeof(buffer), FALSE, FILE_NOTIFY_CHANGE_LAST_WRITE | FILE_NOTIFY_CHANGE_FILE_NAME, &BytesReturned, NULL, NULL) ){
			pInfo = (PFILE_NOTIFY_INFORMATION)buffer;
			while(true){
				CString filename(pInfo->FileName, pInfo->FileNameLength/2);
				if (pInfo->Action == FILE_ACTION_MODIFIED){
					if (modules.find(filename) != modules.end()){
						CWidget *widget = (CWidget*)modules[filename]->m_pMainWnd;
						widget->PostMessageA(WM_RELOAD);
					}
				} else if (pInfo->Action == FILE_ACTION_ADDED || pInfo->Action == FILE_ACTION_RENAMED_NEW_NAME){
					if (filename.Right(3) == CString(".py")) {
						CWidgetThread *pThread = (CWidgetThread*)AfxBeginThread(RUNTIME_CLASS(CWidgetThread), 0, 0, CREATE_SUSPENDED);
						pThread->pt = config[filename];
						if (config.find(filename) != config.end()) pThread->pt = config[filename];
						else {
							pThread->pt = CPoint(5,5);
							config[filename] = CPoint(5,5);
						}
						pThread->file = filename;
						pThread->main = AfxGetMainWnd();
						pThread->ResumeThread();
						modules[filename] = pThread;
					}
				} else if (pInfo->Action == FILE_ACTION_RENAMED_OLD_NAME || pInfo->Action == FILE_ACTION_REMOVED){
					if (filename.Right(3) == CString(".py")) {
						modules[filename]->m_pMainWnd->KillTimer(0);
						modules[filename]->m_pMainWnd->PostMessageA(WM_ACTUALCLOSE);
					}
				}
				if (!pInfo->NextEntryOffset) break;
				pInfo = (FILE_NOTIFY_INFORMATION*)((TCHAR*)pInfo + pInfo->NextEntryOffset);
			}
	}
}

class CTextWidgets : public CWinApp
{
	map<CString, CPoint> config;
	map<CString, CWidgetThread*> modules;
	BOOL InitInstance(){
		CStringA strLine;
		CStdioFile f;
		if (f.Open("C:\\TextWidgets\\widgets.cfg", CFile::modeRead | CFile::shareDenyNone)){
			while (f.ReadString(strLine)){
				if (strLine[0] == L'#') continue;
				if (strLine.IsEmpty()) continue;
				int i = 0;
				CString module = strLine.Tokenize(" ",i);
				int x = atoi(strLine.Tokenize(" ",i));
				int y = atoi(strLine.Tokenize(" ",i));
				config[module] = CPoint(x,y);							 
			}
		}

		SetCurrentDirectory("C:\\TextWidgets");

		this->m_pMainWnd = new CMainWnd(&config);
#ifdef DEBUG
		AllocConsole();
		SetConsoleTitle("TextWidgets!");
		SetConsoleCtrlHandler(HandlerRoutine, TRUE);
		freopen("CONOUT$","w",stdout);
		freopen("CONOUT$","w",stderr);
		freopen( "CONIN$","r",stdin );
#endif
		Py_Initialize();

		PyRun_SimpleString("import sys");
		PyRun_SimpleString("sys.argv = ['textwidgets.exe']");
		PyRun_SimpleString("sys.dont_write_bytecode = True");
		PyRun_SimpleString("sys.stderr = open('err.log', 'w')");
		PyEval_InitThreads();
		PyEval_ReleaseLock();
		
		CFileFind a;
		BOOL working = a.FindFile("C:\\TextWidgets\\*.py");
		while(working){
			working = a.FindNextFile();
			CWidgetThread *pThread = (CWidgetThread*)AfxBeginThread(RUNTIME_CLASS(CWidgetThread), 0, 0, CREATE_SUSPENDED);
			pThread->file = a.GetFileName();
			pThread->main = this->m_pMainWnd;
			if (config.find(pThread->file) != config.end()) pThread->pt = config[pThread->file];
			else {
				pThread->pt = CPoint(5,5);
				config[pThread->file] = CPoint(5,5);
			}
			pThread->ResumeThread();
			modules[a.GetFileName()] = pThread;
		}
		pair<map<CString, CWidgetThread*>*, map<CString, CPoint>* > *p = new pair<map<CString, CWidgetThread*>*, map<CString, CPoint>* >();
		p->first = &modules;
		p->second = &config;
		AfxBeginThread((AFX_THREADPROC)moduleWatch, p);
		return TRUE;
	}
};

CTextWidgets theApp;
