#if !_AFXDLL
#define _AFXDLL
#endif
#define _WIN32_WINNT 0x0503
#include <afxwin.h>
#include <afxtempl.h>
#include <commctrl.h>
#undef _DEBUG
#include <Python.h>
#include "CMemDC.h"
#include "resource.h"
#include <stdlib.h>
#include <map>
#include <io.h>
#include <fcntl.h>
using namespace std;
#ifdef DEBUG
#include <iostream>
#endif DEBUG
#define WM_RELOAD (WM_USER)
#define WM_NOTIFYICON (WM_USER+1)
#define WM_UPDATECONF (WM_USER+2)
#define WM_ACTUALCLOSE (WM_USER+3)